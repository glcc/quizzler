//
//  Question.swift
//  Quizzler
//
//  Created by Gerson  on 05/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

class Question {
    
    let question: String
    let answer: Bool
    
    init(text: String, correctAnswer: Bool) {
        self.question = text
        self.answer = correctAnswer
    }
}
